#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
unsigned long redT = 0;
unsigned long greenT = 0;

void setup() {
  Serial.begin(9600);
  lcd.begin();
  lcd.backlight();

  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, OUTPUT);   //rosu
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);   //albastru
  pinMode(9, OUTPUT);   //verde
  pinMode(10, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(A0, OUTPUT);
  digitalWrite(7, LOW);
  digitalWrite(9, LOW);
  digitalWrite(6, LOW);
  digitalWrite(8, LOW);
  digitalWrite(10, LOW);
  digitalWrite(13, LOW);
  digitalWrite(A0, LOW);

}

void loop() {
  Serial.println("1A");
  Serial.println("2D");
  clearDisplay();
  lcd.setCursor(0, 0);
  lcd.print("1.Atentat");
  lcd.setCursor(0, 1);
  lcd.print("2.Domination");
  lcd.display();
  while (digitalRead(3) && digitalRead(5));
  if (digitalRead(3) == LOW)
    atentat();
  if (digitalRead(5) == LOW)
    domination();
}

void domination()
{
  clearDisplay();
  lcd.setCursor(0, 0);
  lcd.print("R-30min");
  lcd.setCursor(4, 1);
  lcd.print("A-60min");
  lcd.setCursor(9, 0);
  lcd.print("V-90min");
  lcd.display();
  Serial.println("30m");
  Serial.println("60m");
  Serial.println("90m");
  delay(1500);

  while (digitalRead(3) && digitalRead(4) && digitalRead(5));

  if (digitalRead(3) == LOW)
  {
    dominationT(30);
  }
  if (digitalRead(4) == LOW)
  {
    dominationT(60);
  }
  if (digitalRead(5) == LOW)
  {
    dominationT(90);
  }
}

void dominationT(int param)
{
  clearDisplay();
  lcd.setCursor(0, 0);
  switch (param) {
    case 30: lcd.print("30min");
      break;
    case 60: lcd.print("60min");
      break;
    case 90: lcd.print("90min");
      break;
  }
  Serial.println("apasa galben");
  lcd.display();
  delay(1500);
  while (digitalRead(4));
  dominationStart(param);
}
void dominationStart(int param) {
  clearDisplay();
  digitalWrite(6, LOW);
  digitalWrite(8, HIGH);
  digitalWrite(9, LOW);

  lcd.setCursor(0, 0);
  lcd.print("CAMPAIGN ONGOING");
  Serial.println("am inceput");
  lcd.display();
  unsigned long finalt = millis() + (param * 60000);
  while ((millis() < finalt) && digitalRead(3) && digitalRead(5))
  {
    showTime(finalt);
  }
  if (digitalRead(3) == LOW)
    startRed(finalt);
  if (digitalRead(5) == LOW)
    startGreen(finalt);
  if (millis() >= finalt)
    showRes();
}

void startRed(unsigned long finalt)
{
  clearDisplay();
  digitalWrite(6, HIGH);
  digitalWrite(8, LOW);
  digitalWrite(9, LOW);

  unsigned long startt = millis();
  showTime(finalt);
  while (millis() < finalt && digitalRead(5) == HIGH)
  {
    showTime(finalt);
    digitalWrite(13, HIGH);
    delay(600);
    digitalWrite(13, LOW);
  }
  if (digitalRead(5) == LOW)
  {
    redT += millis() - startt;
    Serial.println(redT/60000);
    startGreen(finalt);
  }
  if (millis() >= finalt)
    showRes();
}
void startGreen(unsigned long finalt)
{
  clearDisplay();
  digitalWrite(6, LOW);
  digitalWrite(8, LOW);
  digitalWrite(9, HIGH);

  unsigned long startt = millis();
  while (millis() < finalt && digitalRead(3) == HIGH)
  {
    showTime(finalt);
    digitalWrite(13, HIGH);
    delay(150);
    digitalWrite(13, LOW);
  }
  if (digitalRead(3) == LOW)
  {
    greenT += millis() - startt;
    Serial.println(greenT/60000);
    startRed(finalt);
  }
  if (millis() >= finalt)
    showRes();
}

void showRes()
{
  clearDisplay();
  digitalWrite(6, LOW);
  digitalWrite(8, HIGH);
  digitalWrite(9, LOW);
  lcd.setCursor(0, 0);
  int secR = 0;
  int secG = 0;
  redT = redT/60000;
  greenT = greenT/60000;
  secR = (redT / 1000) % 60;
  secG = (greenT / 1000) % 60;
  int minR = redT / 60000;
  int minG = greenT / 60000;
  String minRs = String(redT);
  lcd.print(minRs);
  lcd.setCursor(8, 0);
  lcd.print("min-RED");
  String minGs = String(greenT);
  lcd.setCursor(0, 1);
  lcd.print(minGs);
  lcd.setCursor(8, 1);
  lcd.print("-GREEN");
  lcd.display();
  while (true);
}

void showTime(unsigned long finalt)
{
  unsigned long remained = finalt - millis();
  int remainedSec = (remained / 1000) % 60;
  remained = remained / 60000;
  String remainedSecS = String(remainedSec);
  String remainedS = String(remained);
  if (remainedSec < 10)
  {
    remainedSecS = "0" + remainedSecS;
  }
  remainedS += ":" + remainedSecS;
  lcd.setCursor(0, 0);
  lcd.print("CAMPAIGN ONGOING");
  lcd.setCursor(0, 8);
  lcd.print(remainedS);
  lcd.display();
}
//-----------------------------------------------------------------------

void atentat() {
  clearDisplay();
  lcd.setCursor(0, 0);
  lcd.print("R-20min");
  lcd.setCursor(4, 1);
  lcd.print("A-40min");
  lcd.setCursor(9, 0);
  lcd.print("V-60min");
  lcd.display();
  delay(1500);
  while (digitalRead(3) && digitalRead(4) && digitalRead(5));

  if (digitalRead(3) == LOW)
  {
    atentatT(1200000);
  }
  if (digitalRead(4) == LOW)
  {
    atentatT(2400000);
  }
  if (digitalRead(5) == LOW)
  {
    atentatT(3600000);
  }
}

void atentatT(unsigned long param)
{
  clearDisplay();
  bool control = true;
  unsigned long currT = 0;
  unsigned long amtime = 0;
  String loading;
  while (control)
  {
    if (digitalRead(5) == LOW)
    {
      clearDisplay();
      loading = "*";
      currT = millis();
      while ((millis() < (currT + 3000)) && digitalRead(5) == LOW)
      {
        clearDisplay();
        lcd.setCursor(0, 0);
        lcd.print(loading);
        lcd.display();
        delay(500);
        loading += "*";
        amtime = millis();
      }
      if (amtime >= currT + 3000)
      {
        control = false;  //amorsat
      }
    }
    
    lcd.setCursor(0, 0);
    lcd.print("DEZAMORSAT");
    lcd.display();
    
  }

  control = true;
  clearDisplay();
  showTimeAtentat(param);
  while (control)
  {
    showTimeAtentat(param);
    if (digitalRead(4) == LOW)
    {
      loading = "*";
      currT = millis();
      while ((millis() < (currT + 5000)) && digitalRead(4) == LOW)
      {
        clearDisplay();
        lcd.setCursor(0, 0);
        lcd.print(loading);
        lcd.display();
        delay(500);
        loading += "*";
        amtime = millis();
      }
      if (amtime >= currT + 5000)
      {
        control = false;  //amorsat
        lcd.setCursor(0, 0);
        lcd.print("DEZAMORSAT");
        lcd.display();
        digitalWrite(8, HIGH);
        digitalWrite(13, HIGH);
        delay(200);
        digitalWrite(13, LOW);
        delay(200);
        digitalWrite(13, HIGH);
        delay(200);
        digitalWrite(13, LOW);
        delay(200);
        digitalWrite(13, HIGH);
        delay(200);
        digitalWrite(13, LOW);
        while (true);
      }
    }

  }
}



void showTimeAtentat(unsigned long finalt)
{
  String remainedS = "";
  digitalWrite(9, HIGH);
  unsigned long remained = finalt - millis();
  int remainedSec = (remained / 1000) % 60;
  remained = remained / 60000;
  String remainedSecS = String(remainedSec);
  if (remainedSec < 10)
  {
    remainedSecS = "0" + remainedSecS;
  }
  remainedS = String(remained) + ":" + remainedSecS;
  digitalWrite(8, LOW);
  lcd.setCursor(0, 0);
  lcd.print("AMORSAT");
  lcd.setCursor(0, 50);
  lcd.print(remainedS);
  lcd.display();

  if (remained > 10)
  {
    digitalWrite(13, HIGH);
    digitalWrite(9, HIGH);
    delay(500);
    digitalWrite(13, LOW);
    digitalWrite(9, LOW);
    delay(700);
  }
  if (remained <= 10)
  {
    digitalWrite(13, HIGH);
    digitalWrite(9, HIGH);
    delay(250);
    digitalWrite(13, LOW);
    digitalWrite(9, LOW);
    delay(250);
  }
  if (remained <= 0)
  {
    digitalWrite(13, HIGH);
    while (true);
  }
}

void clearDisplay()
{
  lcd.setCursor(0, 0);
  lcd.print("                   ");
  lcd.setCursor(0, 1);
  lcd.print("                   ");
  lcd.display();
}
